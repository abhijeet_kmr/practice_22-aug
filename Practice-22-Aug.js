const school = {
  engineering: {
    students: 324,
    dep_id: 3,
    faculties: 4,
  },
  medical: {
    students: 499,
    dep_id: 2,
    faculties: 6,
  },
  "pure-science": {
    students: 133,
    dep_id: 1,
    faculties: 2,
  },
  linguistics: {
    students: 183,
    dep_id: 4,
    faculties: 3,
  },
  philosophy: {
    students: 73,
    dep_id: 5,
    faculties: 2,
  },
};
//  1. Group the data in terms of deparment id.
// { dep_id: { // department details }, dep_id2 : { //department details} ]

let groupedDataWithDeptId = Object.entries(school).reduce((acc, curr) => {
  let id = curr[1].dep_id;
  acc[id] = curr;
  return acc;
}, {});
console.log(groupedDataWithDeptId);

//  2. Filter data with students greater than 200.

let filteredData = Object.keys(school).reduce((acc, curr) => {
  if (school[curr].students > 200) {
    acc[curr] = school[curr];
  }
  return acc;
}, {});
console.log(filteredData);

//  3. Add a new property (labs-required) to each object . Set value as true for engineering medical and purescience ..and false for lingustics and philosophy.

let addedProperty = Object.keys(school).reduce((acc, curr) => {
  let { students, dep_id, faculties } = school[curr];
  if (curr === "lingustics" || curr === "philosophy") {
    acc[curr] = {
      students: students,
      dep_id: dep_id,
      faculties: faculties,
      "lab-required": false,
    };
  } else {
    acc[curr] = {
      students: students,
      dep_id: dep_id,
      faculties: faculties,
      "labs-required": true,
    };
  }
  return acc;
}, {});
console.log(addedProperty);
// console.log(school);
